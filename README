Grip
====

Grip is a cd-player and cd-ripper for the Gnome desktop.. It has the ripping
capabilities of cdparanoia builtin, but can also use external rippers (such
as cdda2wav). It also provides an automated frontend for MP3 encoders,
letting you take a disc and transform it easily straight into MP3s. Internet
disc lookups are supported for retrieving track information from disc
database servers. Grip works with DigitalDJ to provide a unified
"computerized" version of your music collection.


Running Grip
============

Grip's usage is:

  grip [options]

  where the available options are:

    --config=CONFIG             Specify the configuration file to use
                                (defaults to .grip, relative to home dir)
    --device=DEVICE             Specify the cdrom device to use
    --scsi-device=DEVICE        Specify the generic scsi device to use
    --small                     Launch in "small" (cd-only) mode
    --local                     "Local" mode -- do not look up disc info on
                                the net
    --no-redirect               Do not do I/O redirection
    --verbose                   Run in verbose (debug) mode


Getting More Help
================

For more information, see the online documentation within Grip. It can also
be accessed locally in the source distribution. Load doc/C/grip/grip.html
into an html viewer.

If you need more help with Grip, your best resource is the Grip mailing
list. You will find it at:

  https://lists.sourceforge.net/lists/listinfo/grip-users

Be sure to check the mailing list archives for answers to any questions you
may have.

To report a bug with Grip, or to submit a patch. Please do so at the grip
section on sourceforge.net in the tickets area:

  https://sourceforge.net/projects/grip

You can also ask for help on the Freenode channel #grip, where the main developers hang out.
But be prepared to wait a few hours for answers, as we have jobs and a life outside IRC. ;-)
The main developer team members reside in Europe,
so we use the european timezone … surprise, surprise. :-)

Translations
===========

Translations are being managed through the Translation Project.
If you want to help translate Grip, you must do so by 
sending translations through the Translaton Project.
http://translationproject.org/domain/grip.html

For info on how to start, this page is a good start:
http://translationproject.org/html/translators.html

---
Author: Mike Oliphant (oliphant@gtk.org)

Project manager: Johnny A. Solbu (johnny@solbu.net)
