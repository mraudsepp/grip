# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://sourceforge.net/projects/grip\n"
"POT-Creation-Date: 2019-10-05 00:55+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/main.c:56
msgid "Specify the geometry of the main window"
msgstr ""

#: src/main.c:57
msgid "GEOMETRY"
msgstr ""

#: src/main.c:65
msgid "Specify the config file to use (in your home dir)"
msgstr ""

#: src/main.c:66
msgid "CONFIG"
msgstr ""

#: src/main.c:74
msgid "Specify the cdrom device to use"
msgstr ""

#: src/main.c:75 src/main.c:84
msgid "DEVICE"
msgstr ""

#: src/main.c:83
msgid "Specify the generic scsi device to use"
msgstr ""

#: src/main.c:92
msgid "Launch in \"small\" (cd-only) mode"
msgstr ""

#: src/main.c:101
msgid "\"Local\" mode -- do not look up disc info on the net"
msgstr ""

#: src/main.c:110
msgid "Do not do I/O redirection"
msgstr ""

#: src/main.c:119
msgid "Run in verbose (debug) mode"
msgstr ""

#: src/grip.c:186
msgid "Grip"
msgstr ""

#: src/grip.c:240
#, c-format
msgid "Error: Unable to initialize [%s]\n"
msgstr ""

#: src/grip.c:343
msgid ""
"Work is in progress.\n"
"Really shut down?"
msgstr ""

#: src/grip.c:401
msgid "Help"
msgstr ""

#: src/grip.c:406
msgid "Table Of Contents"
msgstr ""

#: src/grip.c:412
msgid "Playing CDs"
msgstr ""

#: src/grip.c:418
msgid "Ripping CDs"
msgstr ""

#: src/grip.c:424
msgid "Configuring Grip"
msgstr ""

#: src/grip.c:430
msgid "FAQ"
msgstr ""

#: src/grip.c:436
msgid "Getting More Help"
msgstr ""

#: src/grip.c:442
msgid "Reporting Bugs"
msgstr ""

#: src/grip.c:462
msgid "About"
msgstr ""

#: src/grip.c:481
#, c-format
msgid "Version %s"
msgstr ""

#: src/grip.c:551
msgid "Error: Trying to show homepage failed."
msgstr ""

#: src/grip.c:830
msgid "Created by Grip"
msgstr ""

#: src/grip.c:848
msgid ""
"Your config file is out of date -- resetting to defaults.\n"
"You will need to re-configure Grip.\n"
"Your old config file has been saved with -old appended."
msgstr ""

#: src/grip.c:934
#, c-format
msgid "server is %s, port %d\n"
msgstr ""

#: src/grip.c:955
msgid "Error: Unable to save config file."
msgstr ""

#: src/gripcfg.c:104
msgid "Config"
msgstr ""

#: src/gripcfg.c:112
msgid "CDRom device"
msgstr ""

#: src/gripcfg.c:117
msgid "Don't interrupt playback on exit/startup"
msgstr ""

#: src/gripcfg.c:121
msgid "Rewind when stopped"
msgstr ""

#: src/gripcfg.c:126
msgid "Startup with first track if not playing"
msgstr ""

#: src/gripcfg.c:131
msgid "Auto-play on disc insert"
msgstr ""

#: src/gripcfg.c:136
msgid "Reshuffle before each playback"
msgstr ""

#: src/gripcfg.c:141
msgid "Work around faulty eject"
msgstr ""

#: src/gripcfg.c:146
msgid "Poll disc drive for new disc"
msgstr ""

#: src/gripcfg.c:150
msgid "Poll interval (seconds)"
msgstr ""

#: src/gripcfg.c:158
msgid "CD"
msgstr ""

#: src/gripcfg.c:174
msgid "Ripper:"
msgstr ""

#: src/gripcfg.c:219
msgid "Ripping executable"
msgstr ""

#: src/gripcfg.c:224
msgid "Rip command-line"
msgstr ""

#: src/gripcfg.c:235
msgid "Disable paranoia"
msgstr ""

#: src/gripcfg.c:240
msgid "Disable extra paranoia"
msgstr ""

#: src/gripcfg.c:246
msgid "Disable scratch"
msgstr ""

#: src/gripcfg.c:250
msgid "detection"
msgstr ""

#: src/gripcfg.c:254
msgid "repair"
msgstr ""

#: src/gripcfg.c:262
msgid "Calculate gain adjustment"
msgstr ""

#: src/gripcfg.c:270
msgid "Rip file format"
msgstr ""

#: src/gripcfg.c:275
msgid "Rip file format (Multi-artist)"
msgstr ""

#: src/gripcfg.c:279
msgid "Generic SCSI device"
msgstr ""

#: src/gripcfg.c:287
msgid "Ripper"
msgstr ""

#: src/gripcfg.c:296
msgid "Rip 'nice' value"
msgstr ""

#: src/gripcfg.c:300
msgid "Max non-encoded .wav's"
msgstr ""

#: src/gripcfg.c:306
msgid "Auto-rip on insert"
msgstr ""

#: src/gripcfg.c:310
msgid "Beep after rip"
msgstr ""

#: src/gripcfg.c:320
msgid "Auto-eject after rip"
msgstr ""

#: src/gripcfg.c:324
msgid "Auto-eject delay"
msgstr ""

#: src/gripcfg.c:332
msgid "Delay before ripping"
msgstr ""

#: src/gripcfg.c:337
msgid "Delay encoding until disc is ripped"
msgstr ""

#: src/gripcfg.c:342
msgid "Stop cdrom drive between tracks"
msgstr ""

#: src/gripcfg.c:346
msgid "Wav filter command"
msgstr ""

#: src/gripcfg.c:351
msgid "Disc filter command"
msgstr ""

#: src/gripcfg.c:359 src/gripcfg.c:504
msgid "Options"
msgstr ""

#: src/gripcfg.c:366 src/cdplay.c:263 src/cdplay.c:376 src/rip.c:95
msgid "Rip"
msgstr ""

#: src/gripcfg.c:381
msgid "Encoder:"
msgstr ""

#: src/gripcfg.c:416
msgid "Encoder executable"
msgstr ""

#: src/gripcfg.c:421
msgid "Encoder command-line"
msgstr ""

#: src/gripcfg.c:427
msgid "Encoder file extension"
msgstr ""

#: src/gripcfg.c:432
msgid "Encoder file format"
msgstr ""

#: src/gripcfg.c:438
msgid "Encode file format (Multi-artist)"
msgstr ""

#: src/gripcfg.c:446
msgid "Encoder"
msgstr ""

#: src/gripcfg.c:456
msgid "Delete .wav after encoding"
msgstr ""

#: src/gripcfg.c:461
msgid "Insert info into SQL database"
msgstr ""

#: src/gripcfg.c:465
msgid "Create .m3u files"
msgstr ""

#: src/gripcfg.c:470
msgid "Use relative paths in .m3u files"
msgstr ""

#: src/gripcfg.c:474
msgid "M3U file format"
msgstr ""

#: src/gripcfg.c:479
msgid "Encoding bitrate (kbits/sec)"
msgstr ""

#: src/gripcfg.c:483
msgid "Number of CPUs to use"
msgstr ""

#: src/gripcfg.c:487
msgid "Encoder 'nice' value"
msgstr ""

#: src/gripcfg.c:491
msgid "Encoder filter command"
msgstr ""

#: src/gripcfg.c:496
msgid "Execute command after encode"
msgstr ""

#: src/gripcfg.c:511
msgid "Encode"
msgstr ""

#: src/gripcfg.c:520
msgid "Add ID3 tags to encoded files"
msgstr ""

#: src/gripcfg.c:526
msgid "Add ID3v2 tags to encoded files"
msgstr ""

#: src/gripcfg.c:532
msgid "Only tag files ending in '.mp3'"
msgstr ""

#: src/gripcfg.c:536
msgid "ID3 comment field"
msgstr ""

#: src/gripcfg.c:541
msgid "ID3v1 Character set encoding"
msgstr ""

#: src/gripcfg.c:547
msgid "ID3v2 Character set encoding"
msgstr ""

#: src/gripcfg.c:555
msgid "ID3"
msgstr ""

#: src/gripcfg.c:572 src/gripcfg.c:592
msgid "DB server"
msgstr ""

#: src/gripcfg.c:576 src/gripcfg.c:596
msgid "CGI path"
msgstr ""

#: src/gripcfg.c:583
msgid "Primary Server"
msgstr ""

#: src/gripcfg.c:603
msgid "Secondary Server"
msgstr ""

#: src/gripcfg.c:613
msgid "DB Submit email"
msgstr ""

#: src/gripcfg.c:618
msgid "DB Character set encoding"
msgstr ""

#: src/gripcfg.c:623
msgid "Use freedb extensions"
msgstr ""

#: src/gripcfg.c:628
msgid "Perform disc lookups automatically"
msgstr ""

#: src/gripcfg.c:636
msgid "DiscDB"
msgstr ""

#: src/gripcfg.c:645
msgid "Use proxy server"
msgstr ""

#: src/gripcfg.c:652
msgid "Get server from 'http_proxy' env. var"
msgstr ""

#: src/gripcfg.c:656
msgid "Proxy server"
msgstr ""

#: src/gripcfg.c:660
msgid "Proxy port"
msgstr ""

#: src/gripcfg.c:664
msgid "Proxy username"
msgstr ""

#: src/gripcfg.c:670
msgid "Proxy password"
msgstr ""

#: src/gripcfg.c:678
msgid "Proxy"
msgstr ""

#: src/gripcfg.c:687
msgid "Email address"
msgstr ""

#: src/gripcfg.c:691
msgid "CD update program"
msgstr ""

#: src/gripcfg.c:696
msgid "Do not lowercase filenames"
msgstr ""

#: src/gripcfg.c:701
msgid "Allow high bits in filenames"
msgstr ""

#: src/gripcfg.c:706
msgid "Replace incompatible characters by hexadecimal numbers"
msgstr ""

#: src/gripcfg.c:711
msgid "Do not change spaces to underscores"
msgstr ""

#: src/gripcfg.c:716
msgid "Write m3u files using non-posix compliant '\\'s"
msgstr ""

#: src/gripcfg.c:721
msgid ""
"Characters to not strip\n"
"in filenames"
msgstr ""

#: src/gripcfg.c:726
msgid "Show tray icon"
msgstr ""

#: src/gripcfg.c:733
msgid "Misc"
msgstr ""

#: src/gripcfg.c:839
msgid "Error: Unable to save ripper config."
msgstr ""

#: src/gripcfg.c:913
msgid "Error: Unable to save encoder config."
msgstr ""

#: src/cddev.c:174 src/cddev.c:604
#, c-format
msgid "Drive status is %d\n"
msgstr ""

#: src/cddev.c:176
msgid "Drive doesn't support drive status check (assume CDS_NO_INFO)\n"
msgstr ""

#: src/cddev.c:179
msgid "No disc\n"
msgstr ""

#: src/cddev.c:284 src/cddev.c:293 src/cddev.c:310 src/cddev.c:337
msgid "Error: Failed to read disc contents\n"
msgstr ""

#: src/cddev.c:607
msgid "Drive doesn't support drive status check\n"
msgstr ""

#: src/cddev.c:627 src/cddev.c:632
#, c-format
msgid "Unlock failed: %d"
msgstr ""

#: src/cddev.c:636
msgid "CDIOCEJECT"
msgstr ""

#: src/cdplay.c:90
msgid "Cannot do lookup while ripping."
msgstr ""

#. "misc"
#: src/cdplay.c:127
msgid "Unknown Disc"
msgstr ""

#: src/cdplay.c:131
#, c-format
msgid "Track %02d"
msgstr ""

#: src/cdplay.c:234
msgid "Done\n"
msgstr ""

#: src/cdplay.c:237
msgid "Error saving disc data\n"
msgstr ""

#: src/cdplay.c:254 src/cdplay.c:365
msgid "Length"
msgstr ""

#: src/cdplay.c:309
msgid "Tracks"
msgstr ""

#: src/cdplay.c:326
msgid "Select previous CDDB entry."
msgstr ""

#: src/cdplay.c:334
msgid "Select next CDDB entry."
msgstr ""

#: src/cdplay.c:354
msgid "Track"
msgstr ""

#: src/cdplay.c:441
msgid "DoNextDiscDBEntry.\n"
msgstr ""

#: src/cdplay.c:444 src/cdplay.c:471
msgid "No disc data. Returning.\n"
msgstr ""

#: src/cdplay.c:449
msgid "Already at last index. Returning.\n"
msgstr ""

#: src/cdplay.c:458 src/cdplay.c:485
msgid "Done.\n"
msgstr ""

#: src/cdplay.c:468
msgid "DoPrevDiscDBEntry.\n"
msgstr ""

#: src/cdplay.c:476
msgid "Already at first index. Returning.\n"
msgstr ""

#: src/cdplay.c:712 src/discedit.c:398
msgid "Error saving disc data."
msgstr ""

#: src/cdplay.c:783
msgid "Rotate play mode"
msgstr ""

#: src/cdplay.c:803
msgid "Toggle loop play"
msgstr ""

#: src/cdplay.c:881 src/rip.c:230
msgid "Rip status"
msgstr ""

#: src/cdplay.c:948
msgid "Play track / Pause play"
msgstr ""

#: src/cdplay.c:959
msgid "Rewind"
msgstr ""

#: src/cdplay.c:970
msgid "FastForward"
msgstr ""

#: src/cdplay.c:980
msgid "Go to previous track"
msgstr ""

#: src/cdplay.c:989
msgid "Go to next track"
msgstr ""

#: src/cdplay.c:998
msgid "Toggle play mode options"
msgstr ""

#: src/cdplay.c:1008
msgid "Next Disc"
msgstr ""

#: src/cdplay.c:1022
msgid "Stop play"
msgstr ""

#: src/cdplay.c:1032
msgid "Eject disc"
msgstr ""

#: src/cdplay.c:1041
msgid "Scan Disc Contents"
msgstr ""

#: src/cdplay.c:1049
msgid "Toggle Volume Control"
msgstr ""

#: src/cdplay.c:1058
msgid "Toggle disc editor"
msgstr ""

#: src/cdplay.c:1068
msgid "Initiate/abort DiscDB lookup"
msgstr ""

#: src/cdplay.c:1078
msgid "Toggle track display"
msgstr ""

#: src/cdplay.c:1085
msgid "Exit Grip"
msgstr ""

#: src/cdplay.c:1252
msgid "Cannot fast forward while ripping."
msgstr ""

#: src/cdplay.c:1283
msgid "Cannot rewind while ripping."
msgstr ""

#: src/cdplay.c:1314
msgid "Cannot switch discs while ripping."
msgstr ""

#: src/cdplay.c:1334
msgid "Cannot eject while ripping."
msgstr ""

#: src/cdplay.c:1344
msgid "Have disc -- ejecting\n"
msgstr ""

#: src/cdplay.c:1411
msgid "Cannot play while ripping."
msgstr ""

#: src/cdplay.c:1479 src/cdplay.c:1510
msgid "Cannot switch tracks while ripping."
msgstr ""

#: src/cdplay.c:1601
msgid "Checking for a new disc\n"
msgstr ""

#: src/cdplay.c:1606
msgid "CDStat found a disc, checking tracks\n"
msgstr ""

#: src/cdplay.c:1609
msgid "We have a valid disc!\n"
msgstr ""

#: src/cdplay.c:1636
msgid "No non-zero length tracks\n"
msgstr ""

#: src/cdplay.c:1645
msgid "CDStat said no disc\n"
msgstr ""

#: src/cdplay.c:1794 src/cdplay.c:1816
#, c-format
msgid "Current sector: %6d"
msgstr ""

#: src/cdplay.c:1917
msgid "No Disc"
msgstr ""

#: src/cdplay.c:1969
msgid ""
"This disc has been found on your secondary server,\n"
"but not on your primary server.\n"
"\n"
"Do you wish to submit this disc information?"
msgstr ""

#: src/cdplay.c:1998 src/cdplay.c:2007
msgid "Error: Unable to create temporary file."
msgstr ""

#: src/cdplay.c:2025
msgid "Error: Unable to write disc data."
msgstr ""

#: src/cdplay.c:2034
#, c-format
msgid "Mailing entry to %s\n"
msgstr ""

#: src/cdplay.c:2039
msgid "Error: Unable to execute mail command."
msgstr ""

#: src/dialog.c:44
msgid "System Message"
msgstr ""

#: src/discdb.c:209
#, c-format
msgid "URI is %s\n"
msgstr ""

#: src/discdb.c:290
#, c-format
msgid "Query is [%s]\n"
msgstr ""

#: src/discdb.c:792
#, c-format
msgid "Stat error %d on %s\n"
msgstr ""

#: src/discdb.c:796
#, c-format
msgid "Creating directory %s\n"
msgstr ""

#: src/discdb.c:801
#, c-format
msgid "Error: %s exists, but is a file\n"
msgstr ""

#: src/discdb.c:808
#, c-format
msgid "Error: Unable to open %s for writing\n"
msgstr ""

#: src/discedit.c:61
msgid "Disc title"
msgstr ""

#: src/discedit.c:62
msgid "Disc artist"
msgstr ""

#: src/discedit.c:63
msgid "ID3 genre:"
msgstr ""

#: src/discedit.c:64
msgid "Disc year"
msgstr ""

#: src/discedit.c:65
msgid "Track name"
msgstr ""

#: src/discedit.c:66
msgid "Track artist"
msgstr ""

#: src/discedit.c:92
msgid "W"
msgstr ""

#: src/discedit.c:221
msgid "Split:"
msgstr ""

#: src/discedit.c:225
msgid "Title/Artist"
msgstr ""

#: src/discedit.c:232
msgid "Artist/Title"
msgstr ""

#: src/discedit.c:240
msgid "Split chars"
msgstr ""

#: src/discedit.c:262
msgid "Multi-artist"
msgstr ""

#: src/discedit.c:273
msgid "Save disc info"
msgstr ""

#: src/discedit.c:282
msgid "Submit disc info"
msgstr ""

#: src/discedit.c:401
msgid "No disc present."
msgstr ""

#: src/discedit.c:553
msgid "Cannot submit. No disc is present."
msgstr ""

#: src/discedit.c:568
msgid "You must enter a disc title."
msgstr ""

#: src/discedit.c:575
msgid "You must enter a disc artist."
msgstr ""

#: src/discedit.c:585
msgid ""
"You are about to submit this disc information\n"
"to a commercial CDDB server, which will then\n"
"own the data that you submit. These servers make\n"
"a profit out of your effort. We suggest that you\n"
"support free servers instead.\n"
"\n"
"Continue?"
msgstr ""

#: src/discedit.c:594
msgid ""
"You are about to submit this\n"
"disc information via email.\n"
"\n"
"Continue?"
msgstr ""

#: src/discedit.c:611
msgid "Genre selection"
msgstr ""

#: src/discedit.c:615
msgid ""
"Submission requires a genre other than 'unknown'\n"
"Please select a DiscDB genre below"
msgstr ""

#: src/discedit.c:625
msgid "DiscDB genre"
msgstr ""

#: src/discedit.c:645
msgid "Submit"
msgstr ""

#: src/discedit.c:656
msgid "Cancel"
msgstr ""

#. Doh!
#: src/id3.c:335
msgid "unknown ID3 field\n"
msgstr ""

#: src/launch.c:121
#, c-format
msgid "Error: unable to translate filename. No such user as %s\n"
msgstr ""

#: src/launch.c:312
msgid "Exec failed\n"
msgstr ""

#: src/parsecfg.c:58
msgid "Error: Bad entry type\n"
msgstr ""

#: src/parsecfg.c:81
msgid "Error: Reading config file\n"
msgstr ""

#: src/parsecfg.c:87
msgid "Error: Invalid config file\n"
msgstr ""

#: src/rip.c:104
msgid "Rip+Encode"
msgstr ""

#: src/rip.c:106
msgid "Rip and encode selected tracks"
msgstr ""

#: src/rip.c:112
msgid "Rip Only"
msgstr ""

#: src/rip.c:114
msgid "Rip but do not encode selected tracks"
msgstr ""

#: src/rip.c:120
msgid "Abort Rip and Encode"
msgstr ""

#: src/rip.c:122
msgid "Kill all active rip and encode processes"
msgstr ""

#: src/rip.c:130
msgid "Abort Ripping Only"
msgstr ""

#: src/rip.c:132
msgid "Kill rip process"
msgstr ""

#: src/rip.c:138
msgid "DDJ Scan"
msgstr ""

#: src/rip.c:140
msgid "Insert disc information into the DigitalDJ database"
msgstr ""

#: src/rip.c:155
msgid "Rip partial track"
msgstr ""

#: src/rip.c:166 src/tray.c:186
msgid "Play"
msgstr ""

#: src/rip.c:173
msgid "Current sector:      0"
msgstr ""

#: src/rip.c:204 src/rip.c:221 src/rip.c:269 src/rip.c:918 src/rip.c:970
#: src/rip.c:972 src/rip.c:1123 src/rip.c:1124
msgid "Rip: Idle"
msgstr ""

#: src/rip.c:209
msgid "Enc: Trk 99 (99.9x)"
msgstr ""

#: src/rip.c:240 src/rip.c:282 src/rip.c:1095 src/rip.c:1109
msgid "Enc: Idle"
msgstr ""

#: src/rip.c:264
msgid "Overall indicators:"
msgstr ""

#: src/rip.c:327
msgid "Start sector"
msgstr ""

#: src/rip.c:332
msgid "End sector"
msgstr ""

#: src/rip.c:606
msgid "Error: can't open m3u file."
msgstr ""

#: src/rip.c:700
msgid "In KillRip\n"
msgstr ""

#: src/rip.c:726
#, c-format
msgid "Now total enc size is: %d\n"
msgstr ""

#: src/rip.c:881
#, c-format
msgid "Rip: Trk %d (%3.1fx)"
msgstr ""

#: src/rip.c:914
#, c-format
msgid "Rip: %6.2f%%"
msgstr ""

#: src/rip.c:959
#, c-format
msgid "Rip partial %d  num wavs %d\n"
msgstr ""

#: src/rip.c:962
#, c-format
msgid "Next track is %d, total is %d\n"
msgstr ""

#: src/rip.c:968
msgid "Check if we need to rip another track\n"
msgstr ""

#: src/rip.c:1010
#, c-format
msgid "Enc: Trk %d (%3.1fx)"
msgstr ""

#: src/rip.c:1028
#, c-format
msgid "Enc: %6.2f%%"
msgstr ""

#: src/rip.c:1239 src/rip.c:1245 src/rip.c:1284
msgid "NoArtist"
msgstr ""

#: src/rip.c:1250
msgid "NoTitle"
msgstr ""

#: src/rip.c:1346
msgid ""
"No disc was detected in the drive. If you have a disc in your drive, please "
"check your CDRom device setting under Config->CD."
msgstr ""

#: src/rip.c:1359
msgid ""
"Invalid rip executable.\n"
"Check your rip config, and ensure it specifies the full path to the ripper "
"executable."
msgstr ""

#: src/rip.c:1370
msgid ""
"Invalid encoder executable.\n"
"Check your encoder config, and ensure it specifies the full path to the "
"encoder executable."
msgstr ""

#: src/rip.c:1406
msgid ""
"No tracks selected.\n"
"Rip whole CD?\n"
msgstr ""

#: src/rip.c:1432
msgid "Ripping whole CD\n"
msgstr ""

#: src/rip.c:1472
msgid "In RipNextTrack\n"
msgstr ""

#: src/rip.c:1479
#, c-format
msgid "First checked track is %d\n"
msgstr ""

#: src/rip.c:1489
msgid "Ripping away!\n"
msgstr ""

#: src/rip.c:1539
msgid "No write access to write wav file"
msgstr ""

#: src/rip.c:1554
#, c-format
msgid "Rip: Trk %d (0.0x)"
msgstr ""

#: src/rip.c:1577 src/rip.c:1827
msgid "Out of space in output directory"
msgstr ""

#: src/rip.c:1653
msgid "Calling CDPRip\n"
msgstr ""

#: src/rip.c:1735
#, c-format
msgid "Added track %d to %s list\n"
msgstr ""

#: src/rip.c:1762
msgid "No free cpus\n"
msgstr ""

#: src/rip.c:1778
#, c-format
msgid "Enc track %d\n"
msgstr ""

#: src/rip.c:1803
msgid "No write access to write encoded file."
msgstr ""

#: src/rip.c:1816
#, c-format
msgid "Enc: Trk %d (0.0x)"
msgstr ""

#: src/rip.c:1883
msgid "In CalculateAll\n"
msgstr ""

#: src/rip.c:1889
msgid "We aren't ripping now, so let's zero encoding values\n"
msgstr ""

#: src/rip.c:1905
#, c-format
msgid "Total rip size is: %d\n"
msgstr ""

#: src/rip.c:1906
#, c-format
msgid "Total enc size is: %d\n"
msgstr ""

#: src/tray.c:57
msgid "Artist"
msgstr ""

#: src/tray.c:58
msgid "Title"
msgstr ""

#: src/tray.c:83
#, c-format
msgid ""
"%s - %s\n"
"%02d:%02d of %02d:%02d"
msgstr ""

#: src/tray.c:87
#, c-format
msgid "Ripping  Track %02d:\t%6.2f%% (%6.2f%% )"
msgstr ""

#: src/tray.c:88
#, c-format
msgid "Encoding Track %02d:\t%6.2f%% (%6.2f%% )"
msgstr ""

#: src/tray.c:89
#, c-format
msgid ""
"%s - %s\n"
"%s%s%s"
msgstr ""

#: src/tray.c:92
#, c-format
msgid ""
"%s - %s\n"
"Idle"
msgstr ""

#: src/tray.c:191
msgid "Pause"
msgstr ""

#: src/tray.c:196
msgid "Stop"
msgstr ""

#: src/tray.c:204
msgid "Previous"
msgstr ""

#: src/tray.c:209
msgid "Next"
msgstr ""

#: src/tray.c:217
msgid "Rip and Encode"
msgstr ""

#: src/tray.c:224
msgid "Quit"
msgstr ""
