#!/bin/bash
#
# Generate new icons, when needed.
# License: GNU GPLv3+
# Johnny A. Solbu © 2017
#
convert grip-transparent.png scalable/apps/grip.svg
convert -scale 16 grip-transparent.png 16x16/apps/grip.png
convert -scale 22 grip-transparent.png 22x22/apps/grip.png
convert -scale 24 grip-transparent.png 24x24/apps/grip.png
convert -scale 32 grip-transparent.png 32x32/apps/grip.png
convert -scale 48 grip-transparent.png 48x48/apps/grip.png
convert -scale 64 grip-transparent.png 64x64/apps/grip.png
convert -scale 72 grip-transparent.png 72x72/apps/grip.png
